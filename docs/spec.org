* Game
The player must guess a randomly-chosen five-letter word. They have six tries.
They input a guess letter-by-letter using the keyboard on screen or by typing.
Each guess must be a valid word found in the word list. After a guess is
submitted, the game reveals which letters in the word are correct or incorrect.
There are three possibilities:

1. The letter was not found in the word
2. The letter was found in the word but in a different spot
3. The letter was found in the word in the same spot

In order to win, the player must give a guess that has all five letters in the
correct spots.