import {useState, useEffect} from "react";
import "./Wordle.css";


const words = ["about", "adobe", "round",
               "weary", "pills", "vague"];
const chosenWordArray = Array.from(words[Math.floor(Math.random() * words.length)]);


function Board (props) {
    const emptyRows = Array.from(Array(Math.max(0, 6 - props.guesses.length - 1)));

    return (
        <div className="c-Wordle__board">
            {props.guesses.map(guess => {
                const letterStatuses = guess.map((letter, guessIndex) => {
                    const chosenWordIndex = chosenWordArray.indexOf(letter);
                    if (chosenWordIndex === -1) {
                        return "is-not-correct";
                    } else {
                        if (guessIndex === chosenWordIndex) return "is-correct";
                        else return "is-present";
                    }
                })
                const classNames = letterStatuses.map(status => "c-Wordle__tile " + status);
                return (
                    <div
                        className="c-Wordle__row"
                        key={guess.join("")}
                    >
                        <div className={classNames[0]}>
                            {guess[0]}
                        </div>
                        <div className={classNames[1]}>
                            {guess[1]}
                        </div>
                        <div className={classNames[2]}>
                            {guess[2]}
                        </div>
                        <div className={classNames[3]}>
                            {guess[3]}
                        </div>
                        <div className={classNames[4]}>
                            {guess[4]}
                        </div>
                    </div>
                );
            })}
            {props.guesses.length < 6 && (
                <div className="c-Wordle__row">
                    <div className="c-Wordle__tile">
                        {props.currentGuess[0]}
                    </div>
                    <div className="c-Wordle__tile">
                        {props.currentGuess[1]}
                    </div>
                    <div className="c-Wordle__tile">
                        {props.currentGuess[2]}
                    </div>
                    <div className="c-Wordle__tile">
                        {props.currentGuess[3]}
                    </div>
                    <div className="c-Wordle__tile">
                        {props.currentGuess[4]}
                    </div>
                </div>
            )}
            {emptyRows.map((_, index) => (
                <div
                    className="c-Wordle__row"
                    key={index}
                >
                    <div className="c-Wordle__tile"></div>
                    <div className="c-Wordle__tile"></div>
                    <div className="c-Wordle__tile"></div>
                    <div className="c-Wordle__tile"></div>
                    <div className="c-Wordle__tile"></div>
                </div>
            ))}
        </div>
    );
}


function Keyboard (props) {
    return (
        <div className="c-Wordle__keyboard">
            <div className="c-Wordle__keyboard__row">
                {["q", "w", "e", "r", "t", "y", "u", "i", "o", "p"].map(letter => (
                    <button
                        key={letter}
                        className="c-Wordle__keyboard__button"
                        onClick={() => props.onChar(letter)}
                    >
                            {letter}
                    </button>
                ))}
            </div>
            <div className="c-Wordle__keyboard__row">
                {["a", "s", "d", "f", "g", "h", "j", "k", "l"].map(letter => (
                    <button
                        key={letter}
                        className="c-Wordle__keyboard__button"
                        onClick={() => props.onChar(letter)}
                    >
                            {letter}
                    </button>
                ))}
            </div>
            <div className="c-Wordle__keyboard__row">
                <button
                    className="c-Wordle__keyboard__button  one-and-a-half"
                    onClick={() => props.onEnter()}
                >
                    enter
                </button>
                {["z", "x", "c", "v", "b", "n", "m"].map(letter => (
                    <button
                        key={letter}
                        className="c-Wordle__keyboard__button"
                        onClick={() => props.onChar(letter)}
                    >
                        {letter}
                    </button>
                ))}
                <button
                    className="c-Wordle__keyboard__button  one-and-a-half"
                    onClick={() => props.onDelete()}
                >
                    <span className="material-icons">backspace</span>
                </button>
            </div>
        </div>
    );
}


function arrayEquals(a, b) {
    return (
        Array.isArray(a) &&
        Array.isArray(b) &&
        a.length === b.length &&
        a.every((val, index) => val === b[index])
    );
}

function isWinningWord (guess) {
    return arrayEquals(guess, chosenWordArray);
}


function Wordle () {
    const [isGameWon, setIsGameWon] = useState(false);

    useEffect(() => {
        if (isGameWon) {
            alert("You won!");
        }
    }, [isGameWon]);

    const [guesses, setGuesses] = useState([]);
    const [currentGuess, setCurrentGuess] = useState([]);

    function setGuessChar (char) {
        if (currentGuess.length < 5 && !isGameWon) {
            setCurrentGuess([...currentGuess, char]);
        }
    }

    function submitGuess () {
        if (currentGuess.length === 5 && guesses.length < 6 && !isGameWon) {
            if (!words.includes(currentGuess.join(""))) {
                alert("Not in word list");
            } else {
                setGuesses([...guesses, currentGuess]);
                setCurrentGuess([]);

                if (isWinningWord(currentGuess)) {
                    setIsGameWon(true);
                }
            }
        }
    }

    function removeGuessChar () {
        if (currentGuess.length > 0 && !isGameWon) {
            setCurrentGuess(currentGuess.slice(0, -1));
        }
    }

    return (
        <main className="c-Wordle">
            <header className="c-Wordle__header">
                Wordle
            </header>
            <div className="c-Wordle__game">
                <div className="c-Wordle__board-container">
                    <Board
                        currentGuess={currentGuess}
                        guesses={guesses}
                        isGameWon={isGameWon}
                        onGameWon={() => setIsGameWon(true)}
                    />
                </div>
                <Keyboard
                    onChar={setGuessChar}
                    onDelete={removeGuessChar}
                    onEnter={submitGuess}
                />
            </div>
        </main>
    );
}

export default Wordle;
